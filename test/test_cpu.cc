/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <gtest/gtest.h>

#include <chrono>
#include <thread>

#include <prplprobe/cpu_global.pb.h>
#include <prplprobe/common.pb.h>

using namespace std::chrono_literals;
using namespace prplprobe::internal::v1;
using namespace kpi::cpu::v1;

extern void cpu_init(void);
extern EventList *getGlobalEventList(void);

TEST(cpu, getGlobalEventList) {
    cpu_init();
    // First occurence is NULL
    EXPECT_EQ(getGlobalEventList(), nullptr);
    // Wait to have different CPU values
    std::this_thread::sleep_for(2s);

    EventList *event_list = getGlobalEventList();
    EXPECT_EQ(event_list->event_size(), 1);

    Event event = event_list->event(0);
    EXPECT_EQ(event.kpiname(), "Cpu");
    EXPECT_EQ(event.kpitype(), "Global");

    CpuGlobal global;
    event.event_data().UnpackTo(&global);

    EXPECT_GE(global.cpu_user(), 0);
    EXPECT_GE(global.cpu_nice(), 0);
    EXPECT_GE(global.cpu_system(), 0);
    EXPECT_GE(global.cpu_idle(), 0);
    EXPECT_GE(global.cpu_iowait(), 0);
    EXPECT_GE(global.cpu_irq(), 0);
    EXPECT_GE(global.cpu_softirq(), 0);
    EXPECT_GE(global.cpu_steal_time(), 0);
    EXPECT_GE(global.cpu_guest_system(), 0);

    delete event_list;
}
